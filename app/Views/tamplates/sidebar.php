<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="<?= base_url('') ?>">
            <img src="<?= base_url('') ?>/assets/img/logo-ct-dark.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold">Admin</span>
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item ">
                <a class="nav-link <?php if(uri_string()=="/admin/kategori" || uri_string()=="/admin/kategori/tambah"){echo "active";}?>" href="<?= base_url('admin/kategori') ?>">
                    <span class="nav-link-text ms-1">Kategori</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  <?php if(uri_string()=="/admin/obat" || uri_string()=="/admin/obat/tambah"){echo "active";}?>" href="<?= base_url('admin/obat') ?>">
                    <span class="nav-link-text ms-1">Obat</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  <?php if(uri_string()=="/admin/kasir" || uri_string()=="/admin/kasir/tambah"){echo "active";}?>" href="<?= base_url('admin/kasir') ?>">
                    <span class="nav-link-text ms-1">Kasir</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  <?php if(uri_string()=="/admin/suplier" || uri_string()=="/admin/suplier/tambah"){echo "active";}?>" href="<?= base_url('admin/suplier') ?>">
                    <span class="nav-link-text ms-1">Suplier</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  <?php if(uri_string()=="/admin/transaksi"|| uri_string()=="/admin/transaksi/tambah"){echo "active";}?>" href="<?= base_url('admin/transaksi') ?>">
                    <span class="nav-link-text ms-1">Transaksi</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
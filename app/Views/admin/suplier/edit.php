<?= $this->extend('tamplates/main') ?>
<?= $this->section('breadcrumb') ?>
<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
  <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
  <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Supplier</li>
</ol>
<h6 class="font-weight-bolder mb-0">Supplier</h6>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">

    <div class="row my-4">
        <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Edit Suplier</h6>
                            <form action="<?=base_url('admin/suplier/edit/'.$suplier['ID'])?>" method="post">

                                <div class="mb-4">
                                    <label>Nama</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="NAMA" value="<?= $suplier['NAMA'] ?>">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <button type="submit" class="btn bg-gradient-dark w-100">Simpan</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection() ?>
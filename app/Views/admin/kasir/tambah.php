<?= $this->extend('tamplates/main') ?>
<?= $this->section('breadcrumb') ?>
<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Dashboard</li>
</ol>
<h6 class="font-weight-bolder mb-0">Dashboard</h6>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">

    <div class="row my-4">
        <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Tambahkan Kasir</h6>
                            <form action="<?= base_url('admin/kasir/tambah') ?>" method="post" enctype="multipart/form-data">

                                <div class="mb-4">
                                    <label>Nama</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="NAMA">
                                    </div>
                                    <label>Telp</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="TELP">
                                    </div>
                                    <label>Email</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="EMAIL">
                                    </div>
                                    <label>Gender</label>
                                    <div class="input-group">
                                        <input type="radio" placeholder="" name="GENDER" value="1"> Laki Laki
                                        <input type="radio" placeholder="" name="GENDER" value="0"> Perempuan
                                    </div>
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control" placeholder="" name="PASSWORD">
                                    </div>
                                    <label>Alamat</label>
                                    <div class="input-group">
                                        <textarea type="text" class="form-control" placeholder="" rows="3" name="ALAMAT"></textarea>
                                    </div>
                                    <label>Avatar</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" placeholder="" name="AVATAR">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn bg-gradient-dark w-100">Simpan</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection() ?>
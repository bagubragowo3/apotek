<?= $this->extend('tamplates/main') ?>
<?= $this->section('breadcrumb') ?>
<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
  <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
  <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Dashboard</li>
</ol>
<h6 class="font-weight-bolder mb-0">Dashboard</h6>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
        <div class="container-fluid py-4">
            
        <div class="row my-4">
            <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-7">
                                <h6>Transaksi</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="">No</th>
                                        <th class="">Jumlah Obat</th>
                                        <th class="">Total Harga</th>
                                        <th class="">Kembalian</th>
                                        <th class="">Tanggal Transaksi</th>
                                        <th class="">Total Bayar</th>
                                        <th class="">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($transaksi as $key => $value) {?>
                                        <tr>
                                            <td><?=$key+1?></td>
                                            <td> <?=$value['JUMLAH_OBAT']?></td>
                                            <td> <?=$value['TOTAL_HARGA']?></td>
                                            <td> <?=$value['KEMBALIAN']?></td>
                                            <td> <?=$value['TANGGAL_TRANSAKSI']?></td>
                                            <td> <?=$value['TOTAL_BAYAR']?></td>
                                            <td>
                                                <a href="" class="btn btn-warning">Detail</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?= $this->endSection() ?>
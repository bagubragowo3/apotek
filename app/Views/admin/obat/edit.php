<?= $this->extend('tamplates/main') ?>
<?= $this->section('breadcrumb') ?>
<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
  <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
  <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Obat</li>
</ol>
<h6 class="font-weight-bolder mb-0">Obat</h6>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">

    <div class="row my-4">
        <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Edit Obat</h6>
                            <form action="<?=base_url('admin/obat/edit/'.$obat['ID'])?>" method="post" enctype="multipart/form-data">
                                <div class="mb-4">
                                    <label>Suplier</label>
                                    <select class="form-control" required name="ID_SUPLIER">
                                        <option>Pilih Suplier</option>
                                        <?php foreach ($supplier as $key => $value) {?>
                                            <option value="<?=$value['ID']?>" <?php if($value['ID']==$obat['ID_SUPLIER']){echo "selected";}?>><?=$value['NAMA']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="mb-4">
                                    <label>Kategori</label>
                                    <select class="form-control" required name="ID_KATEGORI">
                                        <option>Pilih Kategori</option>
                                        <?php foreach ($kategori as $key => $value) {?>
                                            <option value="<?=$value['ID']?>" <?php if($value['ID']==$obat['ID_KATEGORI']){echo "selected";}?>><?=$value['NAMA']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="mb-4">
                                    <label>Nama</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="NAMA_OBAT" value="<?=$obat['NAMA_OBAT']?>">
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <label>Deskripsi</label>
                                    <div class="input-group">
                                        <textarea type="text" class="form-control" placeholder=""  rows="3" name="DESKRIPSI"><?=$obat['DESKRIPSI']?></textarea>
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="" name="HARGA" value="<?=$obat['HARGA']?>">
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <label>Stok</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="" name="STOK" value="<?=$obat['STOK']?>">
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <label>Nomor SKU</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="" name="NOMOR_SKU" value="<?=$obat['NOMOR_SKU']?>">
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <label>GAMBAR</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" placeholder="" name="GAMBAR">
                                    </div>
                                    <img width="100px" src="<?=base_url('public/assets/images/'.$obat['GAMBAR'])?>"  width=“320” height=“180”>
                                </div>


                                <div class="col-md-12">
                                    <button type="submit" class="btn bg-gradient-dark w-100">Simpan</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection() ?>
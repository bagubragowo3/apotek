<?= $this->extend('tamplates/main') ?>
<?= $this->section('breadcrumb') ?>
<ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
  <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
  <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Obat</li>
</ol>
<h6 class="font-weight-bolder mb-0">Obat</h6>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
        <div class="container-fluid py-4">
            
        <div class="row my-4">
            <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-7">
                                <h6>Obat</h6>
                                <a href="<?=base_url('admin/obat/tambah')?>" class="btn btn-success">Tambah</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="">No</th>
                                        <th class="">Nama</th>
                                        <th class="">Deskripsi</th>
                                        <th class="">Harga</th>
                                        <th class="">Stok</th>
                                        <th class="">Nomor SKU</th>
                                        <th class="">Gambar</th>
                                        <th class="">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($obat as $key => $value) {?>
                                        <tr>
                                            <td><?=$key+1?></td>
                                            <td> <?=$value['NAMA_OBAT']?></td>
                                            <td> <?=$value['DESKRIPSI']?></td>
                                            <td> <?=$value['HARGA']?></td>
                                            <td> <?=$value['STOK']?></td>
                                            <td> <?=$value['NOMOR_SKU']?></td>
                                            <td><img width="100px" src="<?=base_url('public/assets/images/'.$value['GAMBAR'])?>"  width=“320” height=“180”></td>
                                            <td>
                                                <a href="<?=base_url('admin/obat/edit/'.$value['ID'])?>" class="btn btn-info">Edit</a>
                                                <a href="<?=base_url('admin/obat/delete/'.$value['ID'])?>" class="btn btn-danger">Hapus</a> 
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?= $this->endSection() ?>
<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\KategoriObat;

class KategoriController extends BaseController
{
    public function index()
    {
        $kategori = new KategoriObat();
        $data['kategori'] = $kategori->findAll();
        return view('admin/kategori/index', $data);
    }

    public function create()
    {
        // lakukan validasi
        $validation =  \Config\Services::validation();
        $validation->setRules(['NAMA' => 'required']);
        $isDataValid = $validation->withRequest($this->request)->run();

        // jika data valid, simpan ke database
        if($isDataValid){
            $kategori = new KategoriObat();
            $kategori->insert([
                "NAMA" => $this->request->getPost('NAMA')
            ]);
            return redirect('admin/kategori');
        }
		
        return view('admin/kategori/tambah');
    }

    public function edit($id)
    {
        // ambil artikel yang akan diedit
        $kategori = new KategoriObat();
        $data['kategori'] = $kategori->where('id', $id)->first();

        // lakukan validasi data artikel
        $validation =  \Config\Services::validation();
        $validation->setRules([
            'NAMA' => 'required'
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data vlid, maka simpan ke database
        if ($isDataValid) {
            $kategori->update($id, [
                "NAMA" => $this->request->getPost('NAMA'),
            ]);
            return redirect('admin/kategori');
        }

        // tampilkan form edit
        return view('admin/kategori/edit', $data);
    }

    public function delete($id)
    {
        $kategori = new KategoriObat();
        $kategori->delete($id);
        return redirect('admin/kategori');
    }
}

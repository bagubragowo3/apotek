<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\Transaksi;

class TransaksiController extends BaseController
{
    public function index()
    {
        $transaksi = new Transaksi();
        $data['transaksi'] = $transaksi->findAll();
        return view('admin/transaksi/index', $data);
    }
}

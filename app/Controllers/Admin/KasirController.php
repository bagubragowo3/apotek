<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\Kasir;

class KasirController extends BaseController
{
    public function index()
    {
        $kasir = new Kasir();
        $data['kasir'] = $kasir->findAll();
        return view('admin/kasir/index', $data);
    }

    public function create()
    {
        // lakukan validasi
        $validation =  \Config\Services::validation();
        $validation->setRules([
            'NAMA' => 'required',
            'ALAMAT' => 'required',
            'TELP' => 'required',
            'EMAIL' => 'required',
            'GENDER' => 'required',
            'PASSWORD' => 'required'
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data valid, simpan ke database
        if($isDataValid){
            $upload = $this->request->getFile('AVATAR');
            $upload->move(WRITEPATH . '../public/assets/images/');
            $kasir = new Kasir();
            $kasir->insert([
                "NAMA" => $this->request->getPost('NAMA'),
                'ALAMAT' => $this->request->getPost('ALAMAT'), 
                'TELP' => $this->request->getPost('TELP'), 
                'EMAIL' => $this->request->getPost('EMAIL'), 
                'GENDER' => $this->request->getPost('GENDER'), 
                'TOTAL_PENJUALAN' => 0, 
                'TOTAL_TRANSAKSI' => 0,
                'AVATAR' => $upload->getName(), 
                'PASSWORD' => password_hash($this->request->getVar('PASSWORD'), PASSWORD_DEFAULT),
            ]);

            return redirect('admin/kasir');
        }
		
        return view('admin/kasir/tambah');
    }

    public function edit($id)
    {
        $kasir = new Kasir();
        $data['kasir'] = $kasir->where('id', $id)->first();
        // lakukan validasi
        $validation =  \Config\Services::validation();
        $validation->setRules([
            'NAMA' => 'required',
            'ALAMAT' => 'required',
            'TELP' => 'required',
            'EMAIL' => 'required',
            'GENDER' => 'required'
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data valid, simpan ke database
        $password = password_hash($this->request->getVar('PASSWORD'), PASSWORD_DEFAULT);

        if($isDataValid){
            $upload = $this->request->getFile('AVATAR');
            $upload->move(WRITEPATH . '../public/assets/images/');
            $kasir = new Kasir();
            $kasir->update($id, [
                "NAMA" => $this->request->getPost('NAMA'),
                'ALAMAT' => $this->request->getPost('ALAMAT'), 
                'TELP' => $this->request->getPost('TELP'), 
                'EMAIL' => $this->request->getPost('EMAIL'), 
                'GENDER' => $this->request->getPost('GENDER'), 
                'TOTAL_PENJUALAN' => 0, 
                'TOTAL_TRANSAKSI' => 0,
                'AVATAR' => $upload->getName(),
                'PASSWORD' => $password,
                // $kasir->update([
                // ]);
            ]);

            if($this->request->getVar('PASSWORD')){
            }
            return redirect('admin/kasir');
        }
		
        return view('admin/kasir/edit', $data);
    }

    public function delete($id)
    {
        $kasir = new Kasir();
        $kasir->delete($id);
        return redirect('admin/kasir');
    }
}

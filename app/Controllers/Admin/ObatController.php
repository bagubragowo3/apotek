<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\KategoriObat;
use App\Models\Obat;
use App\Models\Suplier;

class ObatController extends BaseController
{
    public function index()
    {
        $obat = new Obat();
        $data['obat'] = $obat->findAll();
        return view('admin/obat/index', $data);
    }

    public function create()
    {
        $supplier = new Suplier();
        $data['supplier'] = $supplier->findAll();
        $kategori = new KategoriObat();
        $data['kategori'] = $kategori->findAll();
        // lakukan validasi
        $validation =  \Config\Services::validation();
        $validation->setRules([
            "NAMA_OBAT" => "required",
            "ID_KATEGORI" => "required",
            "ID_SUPLIER" => "required",
            "DESKRIPSI" => "required",
            "HARGA" => "required",
            "STOK" => "required",
            "NOMOR_SKU" => "required",
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data valid, simpan ke database
        if ($isDataValid) {

            $upload = $this->request->getFile('GAMBAR');
            $upload->move(WRITEPATH . '../public/assets/images/');
            
            $obat = new Obat();
            $obat->insert([
                "NAMA_OBAT" => $this->request->getPost('NAMA_OBAT'),
                "ID_KATEGORI" => $this->request->getPost('ID_KATEGORI'),
                "ID_SUPLIER" => $this->request->getPost('ID_SUPLIER'),
                "DESKRIPSI" => $this->request->getPost('DESKRIPSI'),
                "HARGA" => $this->request->getPost('HARGA'),
                "STOK" => $this->request->getPost('STOK'),
                "NOMOR_SKU" => $this->request->getPost('NOMOR_SKU'),
                "GAMBAR" => $upload->getName(),
            ]);

            return redirect('admin/obat');
        }

        return view('admin/obat/tambah', $data);
    }

    public function edit($id)
    {
        $supplier = new Suplier();
        $data['supplier'] = $supplier->findAll();
        $kategori = new KategoriObat();
        $data['kategori'] = $kategori->findAll();
        // ambil artikel yang akan diedit
        $obat = new Obat();
        $data['obat'] = $obat->where('id', $id)->first();

        // lakukan validasi data artikel
        $validation =  \Config\Services::validation();
        $validation->setRules([
            "NAMA_OBAT" => "required",
            "ID_KATEGORI" => "required",
            "ID_SUPLIER" => "required",
            "DESKRIPSI" => "required",
            "HARGA" => "required",
            "STOK" => "required",
            "NOMOR_SKU" => "required",
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data vlid, maka simpan ke database
        if ($isDataValid) {

            $upload = $this->request->getFile('GAMBAR');
            $upload->move(WRITEPATH . '../public/assets/images/');

            $obat->update($id, [
                "NAMA_OBAT" => $this->request->getPost('NAMA_OBAT'),
                "ID_KATEGORI" => $this->request->getPost('ID_KATEGORI'),
                "ID_SUPLIER" => $this->request->getPost('ID_SUPLIER'),
                "DESKRIPSI" => $this->request->getPost('DESKRIPSI'),
                "HARGA" => $this->request->getPost('HARGA'),
                "STOK" => $this->request->getPost('STOK'),
                "NOMOR_SKU" => $this->request->getPost('NOMOR_SKU'),
                "GAMBAR" => $upload->getName(),
            ]);
            return redirect('admin/obat');
        }

        // tampilkan form edit
        return view('admin/obat/edit', $data);
    }

    public function delete($id)
    {
        $kategori = new Obat();
        $kategori->delete($id);
        return redirect('admin/obat');
    }
}

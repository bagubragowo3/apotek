<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;
use App\Models\Suplier;

class SuplierController extends BaseController
{
    public function index()
    {
        $suplier = new Suplier();
        $data = $suplier->orderBy('id', 'desc')->findAll();
        return json_encode(['message' => 'Data suplier', 'data' => $data, 'code' => 1]);
    }
    
    
    public function create()
    {
        // lakukan validasi
        $validation =  \Config\Services::validation();
        $validation->setRules(['NAMA' => 'required']);
        $isDataValid = $validation->withRequest($this->request)->run();

        // jika data valid, simpan ke database
        if($isDataValid){
            $suplier = new Suplier();
            $suplier->insert([
                "NAMA" => $this->request->getPost('NAMA')
            ]);

            return redirect('admin/suplier');
        }
		
        return view('admin/suplier/tambah');
    }

    public function edit($id)
    {
        // ambil artikel yang akan diedit
        $suplier = new Suplier();
        $data['suplier'] = $suplier->where('id', $id)->first();

        // lakukan validasi data artikel
        $validation =  \Config\Services::validation();
        $validation->setRules([
            'NAMA' => 'required'
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();
        // jika data vlid, maka simpan ke database
        if ($isDataValid) {
            $suplier->update($id, [
                "NAMA" => $this->request->getPost('NAMA'),
            ]);
            return redirect('admin/suplier');
        }

        // tampilkan form edit
        return view('admin/suplier/edit', $data);
    }

    public function delete($id)
    {
        $kategori = new Suplier();
        $kategori->delete($id);
        return redirect('admin/suplier');
    }

}

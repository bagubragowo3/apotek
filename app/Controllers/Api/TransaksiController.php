<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;
use App\Models\DetailTransaksi;
use App\Models\Kasir;
use App\Models\Obat;
use App\Models\Transaksi;

class TransaksiController extends BaseController
{
    public function list()
    {   
        $transaksi = new Transaksi();
        $id = $this->request->getPost('ID');
        $data = $transaksi->where('ID_KASIR', $id)->select('tabel_transaksi.ID, tabel_kasir.NAMA, tabel_transaksi.TOTAL_HARGA, tabel_detail_transaksi.TOTAL, tabel_detail_transaksi.QUANTITY, tabel_transaksi.TANGGAL_TRANSAKSI')
        ->join('tabel_kasir', 'tabel_kasir.ID = tabel_transaksi.ID_KASIR')
        ->join('tabel_detail_transaksi', 'tabel_detail_transaksi.ID_TRANSAKSI = tabel_transaksi.ID')
        ->findAll();
        if($data){
            return $this->response->setJSON(['message' => 'List Riwayat', 'code' => 1, 'list' => $data]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
    }
    
    public function detail()
    {
        $transaksi = new Transaksi();
        $data['transaksi_detail'] = new DetailTransaksi();
        return $this->response->setJSON(['message' => 'Data Detail Transaksi', 'code' => 1, 'list' => $transaksi->where('ID', $this->request->getPost('ID_TRANSAKSI'))->first(),
        'detail_list' => $data['transaksi_detail']->where('ID_TRANSAKSI', $this->request->getPost('ID_TRANSAKSI'))->findAll()
        ]);
    }
    
    public function store()
    {
        $data['transaksi'] = new Transaksi();
        $data['transaksi']->insert([
            'ID_KASIR' => $this->request->getPost('ID'),
            'JUMLAH_OBAT' => $this->request->getPost('JUMLAH_OBAT'),
            'TOTAL_HARGA' => $this->request->getPost('TOTAL_HARGA'),
            'KEMBALIAN' => $this->request->getPost('KEMBALIAN'),
            'TOTAL_BAYAR' => $this->request->getPost('TOTAL_BAYAR'),
            'TANGGAL_TRANSAKSI' => $this->request->getPost('TANGGAL_TRANSAKSI'),
        ]);
        foreach ($this->request->getPost('ID_OBAT') as $key => $value) {
            $obat = new Obat();
            $obat = $obat->where('ID', $value)->first();
            $data['transaksi_detail'] = new DetailTransaksi();
            $data['transaksi_detail']->insert([
                'ID_TRANSAKSI' => $data['transaksi']->getInsertID(),
                'ID_KATEGORI'  => $obat['ID_KATEGORI'],
                'ID_OBAT'      => $value,
                'QUANTITY'     => $this->request->getPost('QUANTITY')[$key],
                'TOTAL'        => $this->request->getPost('QUANTITY')[$key] * $obat['HARGA']
            ]);
        }
        return $this->response->setJSON(['message' => 'Data Detail Transaksi', 'code' => 1, 'list' => $data['transaksi']->where('ID', $data['transaksi']->getInsertID())->first(),
        'detail_list' => $data['transaksi_detail']->where('ID_TRANSAKSI', $data['transaksi']->getInsertID())->findAll()
        ]);
    }
}

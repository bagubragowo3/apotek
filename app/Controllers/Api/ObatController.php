<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;
use App\Models\KategoriObat;
use App\Models\Obat;
use App\Models\Suplier;

class ObatController extends BaseController
{
    public function index()
    {
        $obat = new Obat();
        $data = $obat->where('ID_KATEGORI', $this->request->getPost('ID_KATEGORI'))->orderBy('id', 'desc')->findAll();
        if($data){
            return $this->response->setJSON(['message' => 'List Obat', 'code' => 1, 'obat' =>$data]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
    }
    
    public function detail()
    {
        $obat = new Obat();
        $data = $obat->where('ID', $this->request->getPost('ID_OBAT'))->first();
        $kategori = new KategoriObat();
        $data['NAMA_KATEGORI'] = $kategori->where('ID', $data['ID_KATEGORI'])->first()['NAMA'];
        $suplier = new Suplier();
        $data['NAMA_SUPLIER'] = $suplier->where('ID', $data['ID_SUPLIER'])->first()['NAMA'];
        if($data){
            return $this->response->setJSON(['message' => 'Detail Obat', 'code' => 1, 'obat_detail' =>$data]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
        
    }
}

<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;
use App\Models\Kasir;
use CodeIgniter\Entity\Cast\JsonCast;
use CodeIgniter\HTTP\Request;

class LoginController extends BaseController
{
    public function index()
    {
        // lakukan validasi
        $session = session();
        $validation =  \Config\Services::validation();
        $validation->setRules(['nama' => 'required', 'password' => 'required']);
        $isDataValid = $validation->withRequest($this->request)->run();
        if ($isDataValid) {
            $nama = $this->request->getPost('nama');
            $password = $this->request->getVar('password');
            $kasir = new Kasir();
            $kasir = $kasir->where('NAMA', $nama)->first();
            if ($kasir) {
                $verify_pass = password_verify($password, $kasir['PASSWORD']);
                if ($verify_pass) {
                    $session->set($kasir);
                    return $this->response->setJSON(['data' => $kasir, 'message' => 'Berhasil Login', 'code' => 1]);
                } else {
                    return $this->response->setJSON(['message' => 'Name atau Password salah', 'code' => 0]);
                }
                return $this->response->setJSON(['data' => $kasir, 'message' => 'Berhasil Login', 'code' => 1]);
            } else {
                return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
            }
        } else {
            return $this->response->setJSON(['message' => 'Silahkan masukan username dan password anda', 'code' => 0]);
        }
    }

    public function home()
    {
        $id = $this->request->getPost('ID');
        $kasir = new Kasir();
        $kasir = $kasir->where('ID', $id)->first();
        if($kasir){
            return $this->response->setJSON(['message' => 'Home', 'code' => 1, 'total_transaksi' => $kasir['TOTAL_TRANSAKSI'], 'total_penjualan' => $kasir['TOTAL_PENJUALAN']]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
    }

    public function profile()
    {
        $id = $this->request->getPost('ID');
        $kasir = new Kasir();
        $kasir = $kasir->where('ID', $id)->first();
        if($kasir){
            return $this->response->setJSON(['message' => 'Home', 'code' => 1, 'profile' =>$kasir]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
    }
}

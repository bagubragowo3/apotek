<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;
use App\Models\KategoriObat;

class KategoriController extends BaseController
{
    public function index()
    {
        $kategori = new KategoriObat();
        $data = $kategori->orderBy('id', 'desc')->findAll();
        if($data){
            return $this->response->setJSON(['message' => 'List Kategori', 'code' => 1, 'profile' =>$data]);
        }else{
            return $this->response->setJSON(['message' => 'Data tidak ditemukan', 'code' => 0]);
        }
    }
}

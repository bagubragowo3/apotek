<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelDetailTransaksi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'ID' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ID_TRANSAKSI' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'ID_KATEGORI' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'ID_OBAT' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'QUANTITY' => [
                'type'       => 'INT'
            ],
            'TOTAL' => [
                'type'       => 'INT'
            ],
        ]);
        $this->forge->addKey('ID', true);
        $this->forge->createTable('tabel_detail_transaksi');
    }

    public function down()
    {
        $this->forge->dropTable('tabel_detail_transaksi');
    }
}

<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelKasir extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'ID' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'NAMA' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'ALAMAT' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'TELP' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'EMAIL' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'GENDER' => [
                'type'       => 'BOOLEAN',
            ],
            'TOTAL_PENJUALAN' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
            'TOTAL_TRANSAKSI' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
            'PASSWORD' => [
                'type'       => 'VARCHAR',
                'constraint' => '225',
            ],
        ]);
        $this->forge->addKey('ID', true);
        $this->forge->createTable('tabel_kasir');
    }

    public function down()
    {
        $this->forge->dropTable('tabel_kasir');
    }
}

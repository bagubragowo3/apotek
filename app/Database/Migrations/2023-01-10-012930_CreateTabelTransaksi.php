<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelTransaksi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'ID' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ID_KASIR' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'JUMLAH_OBAT' => [
                'type'       => 'INT'
            ],
            'TOTAL_HARGA' => [
                'type'       => 'INT'
            ],
            'KEMBALIAN' => [
                'type'       => 'INT'
            ],
            'TANGGAL_TRANSAKSI' => [
                'type'       => 'DATE'
            ],
            'TOTAL_BAYAR' => [
                'type'       => 'INT'
            ]
        ]);
        $this->forge->addKey('ID', true);
        $this->forge->createTable('tabel_transaksi');
    }

    public function down()
    {
        $this->forge->dropTable('tabel_transaksi');
    }
}

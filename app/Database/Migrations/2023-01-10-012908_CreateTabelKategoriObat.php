<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelKategoriObat extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'ID' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'NAMA' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ]
        ]);
        $this->forge->addKey('ID', true);
        $this->forge->createTable('tabel_kategori_obat');
    }

    public function down()
    {
        $this->forge->dropTable('tabel_kategori_obat');
    }
}

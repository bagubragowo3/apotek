<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTabelObat extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'ID' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ID_KATEGORI' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'ID_SUPLIER' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
            ],
            'NAMA_OBAT' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'DESKRIPSI' => [
                'type'       => 'TEXT',
                'null'       => true,
            ],
            'HARGA' => [
                'type'       => 'INT'
            ],
            'STOK' => [
                'type'       => 'INT'
            ],
            'GAMABAR' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => true,
            ],
            'NOMOR_SKU' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ]
        ]);
        $this->forge->addKey('ID', true);
        $this->forge->createTable('tabel_obat');
    }

    public function down()
    {
        $this->forge->dropTable('tabel_obat');        
    }
}
